
/*
 * GET home page.
 */

exports.index = function(req, res){
	//console.log("Index called" + req.user.username);

  res.render('index',req.user);
};

exports.partials = function (req, res) {

  var name = req.params.name;
  var username = req.user.username;
  res.render('partials/' + name);
};
exports.feepayment = function (req, res) {

  var name = req.params.name;
  res.render('feepayment/' + name);
};

exports.guestHome=function(req,res){
  res.render('guestpayment',req.user);
}

exports.login = function(req, res){
  res.render('login');
};
