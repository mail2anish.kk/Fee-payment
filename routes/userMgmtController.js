var ctx = require('../config/ctxconfig');
var requestPromise = require('request-promise');
const multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
var xlsx = require('xlsx');
var json2xls = require('json2xls');
const token = 'c0c6a0ffc0df0c516d711fba96b713f7';
const serverEndPoint = 'https://kryptosda.kryptosmobile.com/';
// const serverEndPoint = 'http://localhost:8080/';
const apiForUsers = serverEndPoint + 'kryptosds/nuser/getAllUsers';
const apiForValidationProperties = serverEndPoint + 'kryptosds/nuser/validationProperties/get';
const apiToUpdateValidationProperties = serverEndPoint + 'kryptosds/nuser/validationProperties/update';
const apiToAddUnregisteredUsers = serverEndPoint + 'kryptosds/nuser/addMultipleUnregisteredUsers';
const apiToDeleteUnregisteredUsers = serverEndPoint + 'kryptosds/nuser/deleteMultipleUnregisteredUsers';
const apiToUpdateUnregisteredUsers = serverEndPoint + 'kryptosds/nuser/editUnregisteredUser';

const apiForUserProvisioning = 'http://localhost:8080/' + 'kryptosds/nuser/syncToIDP/get';

const getAuthorizationToken = tenantName => `${token} ${tenantName}`;
module.exports = function(app){
	app.get(ctx.path_context + '/tenant/fetchUserList', (req,res) => {
		
		let query = JSON.parse(req.query.q);
		
		let download = Boolean(req.query.download);
		let urlQuery = '?';
		Object.keys(query).forEach(function(key){
			urlQuery += `${key}=${query[key]}&`
		});
		if (download)
			urlQuery += 'download=true';
		var urlOptions = {
			method: 'GET',
			url: apiForUsers + urlQuery,
			headers: {
				'Authorization': getAuthorizationToken(req.user.username)
			}
		};
		requestPromise(urlOptions)
		.then(response => {
			response = JSON.parse(response);
			console.log('response## ',response)
			if (download) {
				return res.xls("User_details.xlsx", response.data);
			} else {
				return res.send(response);
			}
		})
		.catch(error => {
			res.status(error.statusCode).send(error.error);
		});
	});
	app.get(ctx.path_context + '/tenant/fetchValidationProperties', (req,res) => {
		var urlOptions = {
			method: 'GET',
			url: apiForValidationProperties,
			headers: {
				'Authorization': getAuthorizationToken(req.user.username)
			}
		};
		
		requestPromise(urlOptions)
		.then(response => {
			res.send(response);
		})
		.catch(error => {
			res.status(error.statusCode).send(error.error);
		});
	});
	app.put(ctx.path_context + '/tenant/updateValidationProperties', (req,res) => {
		var urlOptions = {
			method: 'PUT',
			url: apiToUpdateValidationProperties,
			headers: {
				'Authorization': getAuthorizationToken(req.user.username)
			},
			json: req.body
		};
		requestPromise(urlOptions)
		.then(response => {
			res.send(response);
		})
		.catch(error => {
			res.status(error.statusCode).send(error.error);
		});
	});

	var storage = multer.diskStorage({ //multers disk storage settings
		destination: function(req, file, cb) {
			cb(null, './uploads/')
		},
		filename: function(req, file, cb) {
			var datetimestamp = Date.now();
			cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
		}
	});
	var upload = multer({ //multer settings
		storage: storage,
		fileFilter: function(req, file, callback) { //file filter
			if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
				return callback(new Error('Wrong extension type'));
			}
			callback(null, true);
		}
	}).single('file');
	app.post(ctx.path_context + '/tenant/upload', function(req, res) {
		var exceltojson;
		upload(req, res, function(err) {
			if (err) {
				res.json({
					error_code: 1,
					err_desc: err
				});
				return;
			}
			if (!req.file) {
				res.json({
					error_code: 1,
					err_desc: "No file passed"
				});
				return;
			}
			if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
				exceltojson = xlsxtojson;
			} else {
				exceltojson = xlstojson;
			}
			var workbook = xlsx.readFile(req.file.path);
			var sheets = workbook.SheetNames;
			var tenant = req.user.username;
			function saveSheetData(index, sheets, tenant, cb) {
				try {
					if (!(index < sheets.length)) {
						return cb(null, true);
					}
					var sheetName = sheets[index];

					exceltojson({
						input: req.file.path,
						output: null,
						sheet: sheetName,
						lowerCaseHeaders: true
					}, function(err, result) {
	
						if (err) {
							return cb(err, null)
						} else {
							var dataLength = result.length;
							let users = [];
							for(var j = 0; j < dataLength; j++) {
								users.push({
									role: result[j].role,
									email: result[j].email,
									unique_id: result[j].unique_id,
									phone: result[j].phone,
									firstname: result[j].firstname,
									lastname: result[j].lastname,
									course: result[j].course
								});
							}
							var urlOptions = {
								method: 'POST',
								url: apiToAddUnregisteredUsers,
								headers: {
									'Authorization': getAuthorizationToken(req.user.username)
								},
								json: { users } 
							};

							requestPromise(urlOptions)
								.then(response => {
									cb(null, response);
								})
								.catch(error => {
									cb(error);
								});
						}
					});
				} catch (e) {
					res.redirect(ctx.path_context + '/tenant#home');
				}
			}
			saveSheetData(0, sheets, tenant, function(err, data) {
				if (err) {
					res.redirect(ctx.path_context + '/#/usersUpload?upload=d');
				}
				if (data) {
					res.redirect(ctx.path_context + '/#/userManagement');
				}
			})
		})
	});

	app.post(ctx.path_context + '/tenant/addNewUser', function(req, res) {
		let users = [req.body];
		var urlOptions = {
			method: 'POST',
			url: apiToAddUnregisteredUsers,
			headers: {
				'Authorization': getAuthorizationToken(req.user.username)
			},
			json: { users } 
		};
		
		requestPromise(urlOptions)
			.then(response => {
				res.send(response);
			})
			.catch(error => {
				res.status(error.statusCode).send(error.error);
			});
	});

	app.post(ctx.path_context + '/tenant/deleteUser', function(req, res) {
		let users = req.body.users;
		var urlOptions = {
			method: 'POST',
			url: apiToDeleteUnregisteredUsers,
			headers: {
				'Authorization': getAuthorizationToken(req.user.username)
			},
			json: { users } 
		};

		requestPromise(urlOptions)
			.then(response => {
				res.send(response);
			})
			.catch(error => {
				res.status(error.statusCode).send(error.error);
			});
	});

	app.put(ctx.path_context + '/tenant/updateUser', function(req, res) {
		let user = req.body;
		var urlOptions = {
			method: 'PUT',
			url: apiToUpdateUnregisteredUsers,
			headers: {
				'Authorization': getAuthorizationToken(req.user.username)
			},
			json: user 
		};

		requestPromise(urlOptions)
			.then(response => {
				res.send(response);
			})
			.catch(error => {
				res.status(error.statusCode).send(error.error);
			});
	});
	
	app.get(ctx.path_context + '/tenant/api/syncToIDP', (req,res) => {
		var urlOptions = {
			method: 'GET',
			url: apiForUserProvisioning,
			headers: {
				'Authorization': getAuthorizationToken(req.user.username)
			}
		};
		requestPromise(urlOptions)
		.then(response => {
			res.send(response);
		})
		.catch(error => {
			res.status(error.statusCode).send(error.error);
		});
	});

};