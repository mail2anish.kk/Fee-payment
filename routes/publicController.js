var dbUtil = require('../config/dbUtil');
var ctx = require('../config/ctxconfig');

var q = require('q');
const pug = require('pug');
var max_retry = 3;

module.exports.controller = function(app) {
	app.get('/feepayment/fetchTenents',function(req,res){
		var response={};
		dbUtil.getConnection(function(db){
			var accColl= db.collection("accounts");
			accColl.find({role:"TENANT"},{salt:0,hash:0}).toArray(function(err,tenents){
				//tenents = dbUtil.walkCursor(tenents);
				//console.log("tenents",tenents);
				if(err){
					response.status="error";
					response.message="something went wrong";
					res.json(response);
					res.end();
				}else{
					response.status="success";
					response.message="";
					response.data = tenents;
					res.json(response);
					res.end();
				}
			});
		});
	});
}