var	Fawn = require('fawn');
var config = require('./config');

Fawn.init(config.mongoUrl);

module.exports.Fawn =  Fawn;
