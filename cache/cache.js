const NodeCache = require("node-cache");
const ndCache = new NodeCache();
var Cache = {
    set: function(key, value, callback) {
        ndCache.set(key, value, callback);
    },
    get: function(key, callback) {
        ndCache.get(key, callback)
    }
};

module.exports = Cache;



