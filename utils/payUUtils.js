var crypto = require('crypto');
var payUConfig = require('../config/payU');
const $q = require('q');
var payU = function() {
    
    var genarateHash=function(toHash){
        var hash = crypto.createHash('sha512');
        hash.update(toHash);
        return hash.digest('hex');
    };
    var blankIfNull = function(value) {
        return value || "";
    };
    

    this.createHash = function(forward, txnid, amount, productInfo, firstname, email, udf1, udf2, udf3, udf4, udf5, additionalCharges, payUKey, payUSalt) {
        var toHash = null;
        if (forward) {
            toHash = payUKey + '|' + txnid + '|' + amount + '|' + productInfo + '|' + firstname + '|' + email + '|' + blankIfNull(udf1) + '|' + blankIfNull(udf2) + '|' + blankIfNull(udf3) + '|' + blankIfNull(udf4) + '|' + blankIfNull(udf5) + '|' + '|' + '|' + '|' + '|' + '|' + payUSalt + '|' + additionalCharges;
        } else {
            toHash = payUSalt + '|' + txnid + '|' + amount + '|' + productInfo + '|' + firstname + '|' + email + '|' + blankIfNull(udf1) + '|' + blankIfNull(udf2) + '|' + blankIfNull(udf3) + '|' + blankIfNull(udf4) + '|' + blankIfNull(udf5) + '|' + '|' + '|' + '|' + '|' + '|' + payUKey;
        }
        var value=genarateHash(toHash); 
        return value;
    };


    this.createHashForOtherServices=function(payUConfig,command,var1){
        var key=payUConfig.key;
        var salt=payUConfig.salt;
        var toHash = key+"|"+command+"|"+var1+"|"+salt;
        return genarateHash(toHash);
    };
    this.generateRandom = function(bits) {
        var buf = crypto.randomBytes(bits);
        return "EV-" + buf.toString("hex").toUpperCase();
    };
    this.calculateAddionalCharges = function(data, payUConfig) {
    	var amount = data.amount;
    	var add_charges = payUConfig.additional_charges;
        var add_charges_str = '';
        var flag = false;
        for (var key in add_charges) {
            var charge = add_charges[key];
            if (flag == true) {
                add_charges_str += ",";
            } else {
                flag = true;
            }
            var value;
            if (charge.type == 'PERCENT') {
                value = amount * charge.value * 0.01; 
            } else {
                value = charge.value;
            }
            // add service taxes 
            //;
           // console.log("payUConfig",payUConfig);
            //console.log("payUConfi GST",payUConfig.gstrate);

            value = value/(1-payUConfig.gstrate);
            value = value.toFixed(2);
            add_charges_str = add_charges_str + key + ":" + value;
            
        }
        return add_charges_str;
    }
};
module.exports = new payU();