const request = require('request-promise');

const emailURL = "https://kryptos.kryptosmobile.com/gateway/Demo/kryptosemailsender";
var headers = {
	'licenseKey': 'UjppQjdjVjpTOjE0MTYyMDM3OTE4Mjc6VTpha0BjYW1wdXNlYWkub3JnOlQ6MzIwOlA6MTY3'
};

module.exports.sendEmail = (emailData) => {
	const reqOptions = {
		method: 'POST',
		headers: headers,
		uri: emailURL,
		body: emailData,
		json: true
	};
	request(reqOptions).then((response) => {
	}).catch((err) => {
		console.log("error  : ", err);
	});
}