var util = require('util'),
    passport = require('passport-strategy');
var dbUtil = require('../config/dbUtil');    
const request = require('request-promise');
var Account = require('../models/account');
var loginURL = "https://kryptosda.kryptosmobile.com/kryptosds/nuser/auth";

function CustomStrategy(options) {
    passport.Strategy.call(this);
}
util.inherits(CustomStrategy, passport.Strategy);

CustomStrategy.prototype.authenticate = function(req, options) {
    //console.log('Options: ', options);
    var self = this;
    this.name = "student";
    console.log("CustomStrategy called");
    var requestData = req.body;
   // console.log(" requestData ", requestData);
    const reqOptions = {
        method: 'POST',
        uri: loginURL,
        body: requestData,
        json: true
    };
    request(reqOptions).then((response) => {
        if (response.error) {
            console.log("error : ",    response.error);

            self.redirect(options.failureRedirect);
        }
        if (response.success) {
            var user ={
                username: req.body.username,
                tenant: req.body.tenant,
                role: "STUDENT",
                platform:'kryptosmobile',
                firstname:response.userinfo.firstname,
                studentRole:response.userinfo.studentRole,
                userid:response.userinfo._id
            };
    
            self.success(user);

        }
    }).catch(function(err) {
        // Deal with the error
        console.log("err : ", err.error);
        //return self.fail();
        self.redirect(options.failureRedirect);
    });

}
module.exports = CustomStrategy;