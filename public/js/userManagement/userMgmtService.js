var feeModule = angular.module("UserModule", []);

var context = "/feepayment";
var role = angular.element("#role22").val().toLowerCase();
var tenant = angular.element("#tenant").val();
var role22 = '/' + role;
var URLPrefix = context + role22;


feeModule.factory("UserSearch", [function() {

	var UserSearch = function(feeObj) {
		feeObj = feeObj || {};
		var searchQuery = this;
		searchQuery.email = feeObj.email || "";
		searchQuery.firstname = feeObj.firstname || '';
		searchQuery.lastname = feeObj.lastname || '';
		searchQuery.course = feeObj.course || '';

		// pass -1 to fetch all the results 

		searchQuery.pgNo = feeObj.pgNo || 1;
		searchQuery.pgSize = feeObj.pgSize || 100;
		searchQuery.data = feeObj.data || [];
		searchQuery.hasMore = true;
		searchQuery.busy = false;

		searchQuery.reset = function() {
			searchQuery.pgNo = 1;
			searchQuery.pgSize = 100;
			searchQuery.data = [];
			searchQuery.hasMore = true;
			searchQuery.busy = false;
		}

		searchQuery.deleteBlanks = function() {
			var clonedSearch = angular.copy(searchQuery);
			delete clonedSearch.data;
			Object.keys(clonedSearch).forEach(function(key){
				if(!clonedSearch[key] || clonedSearch[key]==''){
					delete clonedSearch[key];
				}
			});
			return clonedSearch;
		}

		searchQuery.buildSearch = function() {
			return encodeURIComponent(JSON.stringify(searchQuery.deleteBlanks()));
		};
	};

	return UserSearch;
}]);

feeModule.service("UserMgmtSvc", ["$http", "$filter", "$rootScope", "UserSearch", function($http, $filter, $rootScope, UserSearch) {


	var UserMgmtSvc = function() {
		var that = this;

		that.studentFeeNext = (feeObj) => {
			if (!that.fees) {
				if (!feeObj || !feeObj.tenant) {
					throw new Error("Tenant is mandatory");
				}
				that.fees = new UserSearch(feeObj);
			}

			return new Promise((resolve, reject) => {
				if (!that.fees.hasMore || that.fees.busy == true) {
					resolve(false);
				}
				that.fees.busy = true;
				$http.get(URLPrefix + "/fetchUserList?q=" + that.fees.buildSearch()).success(function(data) {

					that.fees.data = that.fees.data.concat(data.data);

					if (data.length < that.fees.pgSize) {
						that.fees.hasMore = false;
					}
					that.fees.busy = false;
					that.fees.pgNo += 1;
					resolve(true);
				}).error(function(data) {
					that.fees.busy = false;
					reject("Something went wrong");
				});

			});

		};

		that.search = function() {

			that.fees.reset();
			that.studentFeeNext();
		}

		that.editFee = function(feeObj) {

			return new Promise((resolve, reject) => {
				$http.post(URLPrefix + "/api/saveEntry", feeObj).then(function(res) {
					if (res.message == "success")
						resolve(res);
					else
						reject(res);
				}, function(err) {
					reject(err);
				});
			});

		};

		that.deleteFee = function(feeObj) {
			return new Promise((resolve, reject) => {
				$http.post(URLPrefix + "/api/deletedata", feeObj).then(function(res) {
					if (res.message == "success") {
						resolve(res);
					} else {
						reject(res);
					}
				}, function(err) {
					reject(err);
				})
			});
		};

		that.getValidationProperties = function(){
			return $http.get(URLPrefix + "/fetchValidationProperties").success(function(data) {
				that.validationProperties = data.data.validationKeys;
			});
		}
		that.updateValidationProperties = function(body){
			return $http.put(URLPrefix + "/updateValidationProperties", body).success(function(data) {
				console.log('data=>',data);
			});
		}
	};

	return new UserMgmtSvc();
}]);