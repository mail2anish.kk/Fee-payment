angular.module("portalApp-directives.panel", []).directive("ppanel", function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {collapsible:'@collapsible', title:'@title', settings:'=settings'},
        transclude: true,
        link: function(scope, elem, attrs) {
            console.log(attrs.collapsible);
            scope.collapse = function($event) {
                console.log("collapse clicked");
                var target = $event.target;
                var $BOX_PANEL = $(target).closest('.x_panel'),
                    $ICON = $(target).find('i'),
                    $BOX_CONTENT = $BOX_PANEL.find('.x_content');

                // fix for some div with hardcoded fix class
                if ($BOX_PANEL.attr('style')) {
                    $BOX_CONTENT.slideToggle(200, function(){
                        $BOX_PANEL.removeAttr('style');
                    });
                } else {
                    $BOX_CONTENT.slideToggle(200);
                    $BOX_PANEL.css('height', 'auto');
                }

                $ICON.toggleClass('fa-chevron-up fa-chevron-down');
            }
            /*$('.collapse-link').on('click', function() {
                console.log("collapse clicked");
                var $BOX_PANEL = $(this).closest('.x_panel'),
                    $ICON = $(this).find('i'),
                    $BOX_CONTENT = $BOX_PANEL.find('.x_content');

                // fix for some div with hardcoded fix class
                if ($BOX_PANEL.attr('style')) {
                    $BOX_CONTENT.slideToggle(200, function(){
                        $BOX_PANEL.removeAttr('style');
                    });
                } else {
                    $BOX_CONTENT.slideToggle(200);
                    $BOX_PANEL.css('height', 'auto');
                }

                $ICON.toggleClass('fa-chevron-up fa-chevron-down');
            });*/
            $('.close-link').click(function () {
                var $BOX_PANEL = $(this).closest('.x_panel');

                $BOX_PANEL.remove();
            });
            return elem.removeAttr('title');
        },
        template: function(elems, attrs) {
            var heading;
            heading = "";
            heading = "<h2>{{title}}</h2>\n";
            var collapsible="";
            console.log("Collapsible : " + attrs.collapsible + " title : " + attrs.title);
            collapsible =  "<li  ng-show=\"collapsible\"><a class=\"collapse-link\" ng-click=\"collapse($event);\"><i class=\"fa fa-chevron-up\"></i></a>" +
                    "</li>";
            var content = "<div class=\"x_panel\">" +
                          "<div class=\"x_title\">" + heading +
                "<ul class=\"nav navbar-right panel_toolbox\">" +
                "<li class=\"pull-right\"><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>" +
                "</li>" + collapsible +
                "<li class=\"dropdown pull-right\">" +
                "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>" +
                "<ul class=\"dropdown-menu\" role=\"menu\">" +
                "<li ng-repeat=\"set in settings \"><a href=\"#\">{{set.name}}</a>" +
                "</li>" +
                collapsible +

                "</ul>" +
                "</li>" +

                "</ul>" +
                "<div class=\"clearfix\"></div>" +
                "</div>" +
                "<div class=\"x_content\" ng-transclude>" +
                "</div>" +
                "</div>"
            return content;
        }
    };
});