angular.module('portalApp', [
    'ngRoute',
    /*"mobile-angular-ui",
    "mobile-angular-ui.touch",
    "mobile-angular-ui.scrollable",*/
    'portalApp.controllers',
    'portalApp-directives.panel'
]).
    config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
        $routeProvider.when('/home', {templateUrl: '/portal/partials/home.html', controller: 'HomeCtrl'});
        $routeProvider.when('/login', {templateUrl: '/portal/partials/login.html', controller: 'LoginCtrl'});
        $routeProvider.when('/dashboard/:dbid', {templateUrl: '/portal/partials/dashboard.html', controller: 'DashboardCtrl'});

        $routeProvider.when('/app/:appid/:pageid', {
            templateUrl: '/portal/partials/app.html',
            controller: 'AppCtrl'
        });
        $routeProvider.otherwise({redirectTo: '/home'});
        $httpProvider.defaults.timeout = 5000;
    }]);

var onDeviceReady = function () {
    console.log('called on device ready');
    angular.bootstrap(document, ['portalApp']);
    var handleOrientation = function () {
        if (orientation == 0) {
            if (MyCampusApp.homeScreenDisplayed) {
                MyCampusApp.homeRoute.reload();
            }
        } else if (orientation == 90) {
            if (MyCampusApp.homeScreenDisplayed) {
                MyCampusApp.homeRoute.reload();
            }
        } else if (orientation == -90) {
            if (MyCampusApp.homeScreenDisplayed) {
                MyCampusApp.homeRoute.reload();
            }
        } else if (orientation == 180) {
            if (MyCampusApp.homeScreenDisplayed) {
                MyCampusApp.homeRoute.reload();
            }
        } else {
        }
    }
    window.handleOrientation = handleOrientation;
    window.addEventListener('orientationchange', handleOrientation, false);
}

document.addEventListener('deviceready', onDeviceReady, false);

var appDef = function (appid) {
    var app;
    var i;
    for (i = 0; i < appmetadata.apps.length; i++) {
        var tempApp = appmetadata.apps[i]
        if (tempApp.id == appid || tempApp.name == appid) {
            //alert (tempApp.name);
            app = tempApp;
            break;
        }
    }
    return app;
};
var pageDef = function(appDefinition1, pageId) {
    var pgDet;
    var i;
    var adef = appDefinition1;

    for (i = 0; i < adef.pages.length; i++) {
        var tempPage = adef.pages[i]
        if (tempPage.pageid == pageId) {
            //alert (tempApp.name);
            pgDet = tempPage;
            break;
        }
    }
    return pgDet;
};